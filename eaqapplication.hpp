#ifndef EAQAPPLICATION_HPP
#define EAQAPPLICATION_HPP

#include <QApplication>

// Rudimentary QApplication support for exception
// Catches the exception, prints it in a dialog and suicide
class EAQApplication : public QApplication
{
    Q_OBJECT

protected:
    bool insideHandler;
public:
    explicit EAQApplication(int & argc, char ** argv);

    // QCoreApplication interface
    virtual bool notify(QObject * receiver, QEvent * event);
};

#endif // EAQAPPLICATION_HPP
