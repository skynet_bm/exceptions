SOURCES += \
    exceptions/backtrace/backtrace.cpp \
    exceptions/eaqapplication.cpp

HEADERS +=\
    exceptions/exceptions.hpp\
    exceptions/backtrace/backtrace.h \
    exceptions/eaqapplication.hpp
    exceptions/backtrace/booster_config.h

CONFIG += c++11
win32 {
    LIBS+=-lpsapi\
          -ldbghelp
}

linux {
    LIBS+=-ldl
    QMAKE_LFLAGS+=-rdynamic
}

macx {
    LIBS+=-ldl
    QMAKE_LFLAGS+=-rdynamic
}

