#ifndef EXCEPTIONS_HPP_INCLUDED
#define EXCEPTIONS_HPP_INCLUDED
#include <stdexcept>
#include <QString>
#include <QHash>
#include <QVariant>
#include <QDebug>
#include "backtrace/backtrace.h"

// Exceptions thrown by the GIProtocol library
namespace Exceptions
{
    // Base class - basically std::exception with Qt datatypes, with 
    // stacktrace and some sweet .NET-like facilities
    class Exception
        : public virtual std::exception,
        public virtual booster::backtrace
    {
    protected:
        QString message;
    public:
        // Hack needed to let operator<< bind the temporary created in the throw expression
        mutable QString Source;
        // Convention: all keys beginning with an underscore are reserved for internal use of the class
        mutable QHash<QString, QVariant> Data;

        Exception() {}
        Exception(QString Message) : message(Message) {}

        virtual const char * what() const throw() { return message.toUtf8().data(); }

        virtual QString getMessage() const { return message; }

        QString getTrace() const { return QString::fromStdString(trace()); }

        virtual ~Exception() throw() {}
    };

    // Prettyprint
    inline QDebug operator<<(QDebug os, const std::exception & ex)
    {
        os.nospace()<<"Type: "<<typeid(ex).name()<<
            "\nMessage: "<<ex.what();
        return os;
    }

    inline QDebug operator<<(QDebug os, const Exception & ex)
    {
        os.nospace()<<"Type: "<<typeid(ex).name()<<
            "\nMessage: "<<ex.getMessage()<<
            "\nSource: "<<ex.Source<<
            "\nData: "<<ex.Data;
        os.nospace()<<"\n\n=== Begin stack trace ===\n"<<ex.trace().c_str()<<"\n=== End stack trace ===\n";
        if(dynamic_cast<const std::nested_exception *>(&ex))
        {
            os.nospace()<<"Inner exception:\n";
            try
            {
                std::rethrow_if_nested(ex);
            }
            catch(const Exception & inner)
            {
                os<<inner;
            }
            catch(const std::exception & inner)
            {
                // Heck, this shouldn't even happen!
                os<<inner;
            }
            // If it's not derived from std::exception, well, fuck it.
        }
        return os;
    }

    // Some syntax sugar to add typical information to any Exception-derived class
    struct AddSource
    {
        QString Source;
        AddSource(QString S) : Source(S) {}
    };

    struct AddData
    {
        QString Key;
        QVariant Data;
        AddData(QString K, QVariant D) : Key(K), Data(D) {}
    };

    // Important: the template here is fundamental, otherwise we will throw
    // non-polymorphically
    template<typename E>
    inline const E & operator<<(const E & Ex, const AddData & D)
    {
        Ex.Data[D.Key]=D.Data;
        return Ex;
    }

    template<typename E>
    inline const E & operator<<(const E & Ex, const AddSource & S)
    {
        Ex.Source=S.Source;
        return Ex;
    }

    // Generic IO error
    class IOError : public Exception
    {
    public:

        IOError(QString Message, QString Path1, QString Path2=QString()) : Exception("IO error: "+ Message)
        {
            Data["_Path1"]=Path1;
            Data["_Path2"]=Path2;
        }

        virtual ~IOError() throw() {}


        QString getPath1() const { return Data["_Path1"].toString(); }
        QString getPath2() const { return Data["_Path2"].toString(); }
    };

    // We got a timeout during the communication
    class CommunicationTimeout : public Exception
    {
    public:
        CommunicationTimeout(QString Operation, QString AdditionalData=QString())
            : Exception("Timeout exceeded in operation "+Operation)
        {
            Data["_Operation"]=Operation;
            Data["_AdditionalData"]=AdditionalData;
        }

        QString getOperation() const { return Data["_Operation"].toString(); }
        QString getAdditionalData() const { return Data["_AdditionalData"].toString(); }

        virtual ~CommunicationTimeout() throw() {}
    };

    // We tried to do an operation on an object that does not support it
    class UnsupportedOperation : public Exception
    {
        QString operation;
        QString additionalData;
    public:
        UnsupportedOperation(QString Operation, QString AdditionalData=QString())
            : Exception("Unsupported operation: "+Operation)
        {
            Data["_Operation"]=Operation;
            Data["_AdditionalData"]=AdditionalData;
        }

        QString getOperation() const { return Data["_Operation"].toString(); }
        QString getAdditionalData() const { return Data["_AdditionalData"].toString(); }
        virtual ~UnsupportedOperation() throw() {}
    };

    // An argument that was passed is not valid
    class InvalidArgument : public Exception
    {
        QString argument;
        QString additionalData;
    public:
        InvalidArgument(QString Argument, QString AdditionalData=QString())
            : Exception("Invalid argument " + Argument)
        {
            Data["_Argument"]=Argument;
            Data["_AdditionalData"]=AdditionalData;
        }

        QString getArgument() const { return Data["_Argument"].toString(); }
        QString getAdditionalData() const { return Data["_AdditionalData"].toString(); }

        virtual ~InvalidArgument() throw() {}
    };
}
#endif // EXCEPTIONS_HPP_INCLUDED
