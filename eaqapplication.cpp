#include "eaqapplication.hpp"
#include <QDebug>
#include <QMessageBox>
#include "exceptions.hpp"

EAQApplication::EAQApplication(int & argc, char ** argv) :
    QApplication(argc, argv), insideHandler(false)
{
}


bool EAQApplication::notify(QObject * receiver, QEvent * event)
{
    QString msg;
    try
    {
        return QApplication::notify(receiver, event);
    }
    catch(const Exceptions::Exception & ex)
    {
        QDebug d(&msg);
        d<<ex;
    }
    catch(const std::exception & ex)
    {
        using namespace ::Exceptions;
        QDebug d(&msg);
        d<<ex;
    }
    catch(...)
    {
        msg=tr("Unknown exception");
    }
    //qDebug()<<msg;
    // Avoid recursion
    if(insideHandler)
        return false;
    insideHandler=true;
    QMessageBox::critical(nullptr, tr("Fatal error"),
                          tr("Caught exception in event loop:\n")+msg);
    this->quit();
    insideHandler=false;
    return false;
}
