#ifndef BOOSTER_CONFIG_H_INCLUDED
#define BOOSTER_CONFIG_H_INCLUDED
// Just some settings to keep the extracted files happy
#define BOOSTER_API
#if (defined(__linux) && !defined(__UCLIBC__)) || defined(__APPLE__) || defined(__sun) || defined(__FreeBSD__)
#define BOOSTER_HAVE_EXECINFO
#define BOOSTER_HAVE_UNWIND_BACKTRACE
#endif

#ifdef _MSVC_VER
#define BOOSTER_MSVC
#endif
#ifdef _WIN32
#define BOOSTER_WIN32
#endif
#endif
